# my_morning_trial

This is just a small program attempt to compute $cos(x)$ and $sin(x)$ with the help of the identity 

$$
\lim_{n\to \infty} (1+x/n)^n = e^x
$$

so 

$$
\lim_{n\to \infty} (1+ix/n)^n = e^{ix}=cos(x)+i sin(x)
$$

For now the accuracy is no better than $10^{-8}$ for $n=2^{27}$ and it is somehow 90% longer than the standard library for computing cos and sin. Which is not that bad.


to run, just cargo run in the main you can find in the branch master.
